// flow-typed signature: 4ff225dddbbabe1b1d7d1891f3e5936c
// flow-typed version: <<STUB>>/webpack-dev-server-speedy_v^3.1.1/flow_v0.82.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'webpack-dev-server-speedy'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'webpack-dev-server-speedy' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'webpack-dev-server-speedy/bin/webpack-dev-server' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/client/index.bundle' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/client/index' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/client/live.bundle' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/client/overlay' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/client/socket' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/client/sockjs.bundle' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/lib/createLog' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/lib/OptionsValidationError' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/lib/polyfills' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/lib/Server' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/lib/util/addDevServerEntrypoints' {
  declare module.exports: any;
}

declare module 'webpack-dev-server-speedy/lib/util/createDomain' {
  declare module.exports: any;
}

// Filename aliases
declare module 'webpack-dev-server-speedy/bin/webpack-dev-server.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/bin/webpack-dev-server'>;
}
declare module 'webpack-dev-server-speedy/client/index.bundle.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/client/index.bundle'>;
}
declare module 'webpack-dev-server-speedy/client/index.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/client/index'>;
}
declare module 'webpack-dev-server-speedy/client/live.bundle.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/client/live.bundle'>;
}
declare module 'webpack-dev-server-speedy/client/overlay.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/client/overlay'>;
}
declare module 'webpack-dev-server-speedy/client/socket.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/client/socket'>;
}
declare module 'webpack-dev-server-speedy/client/sockjs.bundle.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/client/sockjs.bundle'>;
}
declare module 'webpack-dev-server-speedy/lib/createLog.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/lib/createLog'>;
}
declare module 'webpack-dev-server-speedy/lib/OptionsValidationError.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/lib/OptionsValidationError'>;
}
declare module 'webpack-dev-server-speedy/lib/polyfills.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/lib/polyfills'>;
}
declare module 'webpack-dev-server-speedy/lib/Server.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/lib/Server'>;
}
declare module 'webpack-dev-server-speedy/lib/util/addDevServerEntrypoints.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/lib/util/addDevServerEntrypoints'>;
}
declare module 'webpack-dev-server-speedy/lib/util/createDomain.js' {
  declare module.exports: $Exports<'webpack-dev-server-speedy/lib/util/createDomain'>;
}
