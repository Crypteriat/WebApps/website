// @flow
import React from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import theme from './theme';
import Routes from './Routes';
import * as S from './App.styled';

type Props = {
  entitled: boolean,
  error500: boolean
};

export class App extends React.Component<Props> {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <S.Container>
          <Routes entitled={this.props.entitled} />
        </S.Container>
      </ThemeProvider>
    );
  }
}

export default compose(
  withRouter,
  connect(state => ({}))
)(App);
