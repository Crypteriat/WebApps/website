// @flow weak
import styled, { injectGlobal } from 'styled-components';
import reset from 'styled-reset';
import theme from './theme';

injectGlobal`
  ${reset}

  *,
  *:before,
  *:after {
    box-sizing: border-box;
  }

  * {
    font-family: ${theme.global.font}
  }

  .video-playing {
    position: relative;
    overflow: hidden !important;
  }

  .ReactModal__Body--open {
    position: relative;
    overflow: hidden !important;
  }
`;

export const Container = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  min-height: 100vh;
  background-color: #fff;
`;
