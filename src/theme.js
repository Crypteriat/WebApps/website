// @flow weak
export default {
  // global theme variables
  global: {
    padding: 15,
    font: "'noto-sans', 'Avenir', 'Helvetica', sans-serif",
    lineHeight: 1.4,
    contentWidth: 1200
  },
  fontSizes: [12, 14, 16, 18, 20, 24, 34, 36, 64, 72],
  fontWeight: {
    bold: 700,
    semiBold: 600,
    medium: 500
  },
  colors: {
    purple: '#212852',
    green: '#00914E',
    yellow: '#fef7ed',
    white: '#fff',
    black: '#000',
    grey: '#d8d8d8',
    bgrey: '#9093a8',
    pink: '#F6B1AA',
    orange: '#ed9622',
    brightOrange: '#ff6f3a',
    red: '#9d2c21'
  },
  carousel: {
    padding: 30,
    contentBg: '#707070',
    videoBg: '#919191',
    color: '#fff'
  }
};
