import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { JSDOM } from 'jsdom';

configure({ adapter: new Adapter() });

global.document = new JSDOM('').window.document;
global.window = document.defaultView;
global.Image = window.Image;

// jest.mock('react-native-config', () => require('./src/utils/config'));
